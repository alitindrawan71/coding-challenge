<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Question4Controller extends Controller
{
    /**
     * @OA\Get(
     *      path="/question-4",
     *      tags={"Question"},
     *      summary="Question 4",
     *      description="Question 4",
     *      operationId="question-4",
     *      @OA\Parameter(
     *          name="a",
     *          in="query",
     *          description="First number",
     *          required=true,
     *          @OA\Schema(
     *             type="integer",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="b",
     *          in="query",
     *          description="Second number",
     *          required=true,
     *          @OA\Schema(
     *             type="integer",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="x",
     *          in="query",
     *          description="Third number",
     *          required=true,
     *          @OA\Schema(
     *             type="integer",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="n",
     *          in="query",
     *          description="Fourth number",
     *          required=true,
     *          @OA\Schema(
     *             type="integer",
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *      ),
     *  ),
    **/
    public function __invoke(Request $request)
    {
        return "Question 4";
    }
}
