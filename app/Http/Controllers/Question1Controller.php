<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Question1Controller extends Controller
{
    /**
     * @OA\Get(
     *      path="/question-1",
     *      tags={"Question"},
     *      summary="Question 1",
     *      description="Question 1",
     *      operationId="question-1",
     *      @OA\Parameter(
     *          name="a",
     *          in="query",
     *          description="First text",
     *          required=true,
     *          @OA\Schema(
     *             type="string",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="b",
     *          in="query",
     *          description="Second text",
     *          required=true,
     *          @OA\Schema(
     *             type="string",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="c",
     *          in="query",
     *          description="Third text",
     *          required=true,
     *          @OA\Schema(
     *             type="string",
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *      ),
     *  ),
    **/
    public function __invoke(Request $request)
    {
        return "Question 1";
    }
}
