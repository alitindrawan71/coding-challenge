<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Question5Controller extends Controller
{
    /**
     * @OA\Get(
     *      path="/question-5",
     *      tags={"Question"},
     *      summary="Question 5",
     *      description="Question 5",
     *      operationId="question-5",
     *      @OA\Parameter(
     *          name="a",
     *          in="query",
     *          description="First number",
     *          required=true,
     *          @OA\Schema(
     *             type="integer",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="b",
     *          in="query",
     *          description="Second number",
     *          required=true,
     *          @OA\Schema(
     *             type="integer",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="c",
     *          in="query",
     *          description="Third number",
     *          required=true,
     *          @OA\Schema(
     *             type="integer",
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *      ),
     *  ),
    **/
    public function __invoke(Request $request)
    {
        return "Question 5";
    }
}
