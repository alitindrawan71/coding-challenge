<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Question2Controller extends Controller
{
    /**
     * @OA\Get(
     *      path="/question-2",
     *      tags={"Question"},
     *      summary="Question 2",
     *      description="Question 2",
     *      operationId="question-2",
     *      @OA\Parameter(
     *          name="x",
     *          in="query",
     *          description="First number",
     *          required=true,
     *          @OA\Schema(
     *             type="integer",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="y",
     *          in="query",
     *          description="Second number",
     *          required=true,
     *          @OA\Schema(
     *             type="integer",
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *      ),
     *  ),
    **/
    public function __invoke(Request $request)
    {
        return "Question 2";
    }
}
