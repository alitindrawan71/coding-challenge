<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Question3Controller extends Controller
{
    /**
     * @OA\Get(
     *      path="/question-3",
     *      tags={"Question"},
     *      summary="Question 3",
     *      description="Question 3",
     *      operationId="question-3",
     *      @OA\Parameter(
     *          name="a",
     *          in="query",
     *          description="First text",
     *          required=true,
     *          @OA\Schema(
     *             type="string",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="b",
     *          in="query",
     *          description="Second text",
     *          required=true,
     *          @OA\Schema(
     *             type="string",
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *      ),
     *  ),
    **/
    public function __invoke(Request $request)
    {
        return "Question 3";
    }
}
