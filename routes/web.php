<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Question1Controller;
use App\Http\Controllers\Question2Controller;
use App\Http\Controllers\Question3Controller;
use App\Http\Controllers\Question4Controller;
use App\Http\Controllers\Question5Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/question-1', Question1Controller::class);
Route::get('/question-2', Question2Controller::class);
Route::get('/question-3', Question3Controller::class);
Route::get('/question-4', Question4Controller::class);
Route::get('/question-5', Question5Controller::class);
