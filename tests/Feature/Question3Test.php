<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class Question3Test extends TestCase
{
    protected $url = '/question-3';

    public function test_case_1()
    {
        $params = [
            'x' => 'djoin',
            'y' => 'niojd',
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals("PALINDROM", $response->getContent());
    }

    public function test_case_2()
    {
        $params = [
            'x' => 'teknik',
            'y' => 'informatika',
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals("TIDAK", $response->getContent());
    }

    public function test_case_3()
    {
        $params = [
            'x' => 'lampu',
            'y' => 'palu',
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals("PALINDROM", $response->getContent());
    }
}
