<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class Question4Test extends TestCase
{
    protected $url = '/question-4';

    public function test_case_1()
    {
        $params = [
            'a' => 1,
            'b' => 2,
            'x' => 3,
            'n' => 4,
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals(2, $response->getContent());
    }

    public function test_case_2()
    {
        $params = [
            'a' => 4,
            'b' => 2,
            'x' => 11,
            'n' => 12,
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals(4, $response->getContent());
    }

    public function test_case_3()
    {
        $params = [
            'a' => 4,
            'b' => 2,
            'x' => 28,
            'n' => 30,
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals("TIDAK MUNGKIN", $response->getContent());
    }
}
