<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class Question5Test extends TestCase
{
    protected $url = '/question-5';

    public function test_case_1()
    {
        $params = [
            'a' => 1,
            'b' => 2,
            'c' => 1,
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals(5000, $response->getContent());
    }

    public function test_case_2()
    {
        $params = [
            'a' => 2,
            'b' => 1,
            'c' => 4,
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals(8000, $response->getContent());
    }

    public function test_case_3()
    {
        $params = [
            'a' => 0,
            'b' => 100,
            'c' => 100,
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals(0, $response->getContent());
    }
}
