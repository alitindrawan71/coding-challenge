<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class Question2Test extends TestCase
{
    protected $url = '/question-2';

    public function test_case_1()
    {
        $params = [
            'x' => 1,
            'y' => 30,
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals(23, $response->getContent());
    }

    public function test_case_2()
    {
        $params = [
            'x' => 1,
            'y' => 100,
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals(73, $response->getContent());
    }

    public function test_case_3()
    {
        $params = [
            'x' => 1000,
            'y' => 1001,
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals("TIDAK ADA", $response->getContent());
    }
}
