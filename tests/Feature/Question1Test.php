<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class Question1Test extends TestCase
{
    protected $url = '/question-1';

    public function test_case_1()
    {
        $params = [
            'a' => 'biu',
            'b' => 'ubi',
            'c' => 'ibu',
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals('ANAGRAM', $response->getContent());
    }

    public function test_case_2()
    {
        $params = [
            'a' => 'saya',
            'b' => 'dia',
            'c' => 'kamu',
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals('BUKAN', $response->getContent());
    }

    public function test_case_3()
    {
        $params = [
            'a' => 'tepat',
            'b' => 'tape',
            'c' => 'peta',
        ];

        $response = $this->get($this->url.'?'.http_build_query($params));

        $this->assertEquals('BUKAN', $response->getContent());
    }
}
